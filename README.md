# NURDS_2
The New Ultimate Roller Derby Scoring Simulator


## About 

This is the source code for NURDS_2. It is still under active development, but you can try it at 
[https://nurds.isabelle-santos.space](https://nurds2.isabelle-santos.space).

This application is intended to be a tool to learn about scoring in roller derby. It
is (heavily) based off of the [NURDS (New Ultimate Roller Derby Simulator)](https://nurds.space) 
developed by Fabien Tricoire.

## License

This code is provided under the GNU General Public License v 3.0. You should have received a copy of
this license along with the code. If not, please refer to [the GNU website](https://www.gnu.org/licenses/).

## Contributing

For bug reports and feature requests, please use the [issue track](https://framagit.org/Moutmout/nurds2/-/issues). 
Pull requests are welcome.
