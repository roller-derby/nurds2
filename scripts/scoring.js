// Everything related to scoring.
document.getElementById("totalScoreA").innerHTML = "under construction";
document.getElementById("lapNumberA").innerHTML = jammers[0].lapNumber;
document.getElementById("lapPointsA").innerHTML = "under construction";
document.getElementById("muCoordsA").innerHTML = "under construction";
document.getElementById("ExitedEzA").innerHTML = "under construction";

// Check if any points are scored when a player is moved.
function pointsScored () {
	for (var i=0; i < jammers.length; i++) { 
		jammerEZ(jammers[i]);
	}
}

// Check the status of the jammer relative to the EZ. Update the lap number 
// as needed. A new lap is counted every time the jammer enters the EZ from 
// the back after having exited it from the front.
function jammerEZ (jammer) {
	// If jammer hasn't exited the EZ and crosses the front edge of the EZ, 
	//  then their status is switched to exitedEZ. If the jammer has exited 
	//  and crosses the back edge of the EZ, then their status is switched
	//  back to (not exitedEZ). 
	// 	When the jammer status is switched to (not exitedEZ), a new lap starts.
	//  Note that if the jammer, for instance, exits the EZ from the *front* then 
	//  re-enters from the *front*, they are still considered to have exited the 
	//  EZ (even though they are in the EZ).
    var jammerMu = skaterMu(jammer);
	document.getElementById("lapNumberA").innerHTML = jammers[0].lapNumber ;
	document.getElementById("totalScoreA").innerHTML = jammers[0].exitedEZ ;

	if ( ! jammer.exitedEZ ) {
		// Compare mu coordinates of jammer and EZ front.
        var EZFrontMu = skaterMu(document.getElementById(packIds[packIds.length-1])) + EZLength ;
        var jammerEZDistance = jammerMu - EZFrontMu ;

		if ( jammerEZDistance > 0 ) { // || jammerEZDistance + trackLength < 1 ) {
		    jammer.exitedEZ = true ;

		    // update total score
		    jammer.totalScore = jammer.totalScore + jammer.lapPoints ;
		}
	} else {
		// Compare mu coordinates of jammer and EZ rear.
		var EZBackMu = skaterMu(document.getElementById(packIds[0])) - EZLength ;
		var jammerEZDistance = EZBackMu - jammerMu ;
		if ( jammerEZDistance > 0 ) { // || jammerEZDistance + trackLength < 1) {
		    jammer.exitedEZ = false ;

		    // update lap number and lap score
		    if (! jammer.lapNumber) {
    		    jammer.lapNumber = 1 ;
		    }
		    jammer.lapNumber = jammer.lapNumber + 1 ;
		    jammer.lapPoints = 0 ;
		}
	}
}

function getTotalScore() {
	
}

function getLapNumber() {
	
}

function getLapPoints() {
	
}
